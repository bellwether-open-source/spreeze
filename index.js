const {reporters, utils} = require('mocha');
const chai = require('chai');
const {Base: {symbols, color}, Spec} = reporters;

const INDENT = '  ';
const CHECKMARK = color('checkmark', '  ' + symbols.ok);0
const ERROR_SYMBOL = color('fail', '  ' + symbols.err);0
const PASS = color('pass', ' %s');
const FAIL = color('fail', ' %s');

const displaySuccessfulAssertion = indent => caption => console.log(
    indent + CHECKMARK + PASS,
    caption
);
const displayFailingAssertion = (indent, caption) => console.log(
    indent + ERROR_SYMBOL + FAIL,
    caption
);

function Spreeze(runner, options) {
    let indentLevel = 0;
    let assertions = [];

    const indent = (offset = 0) => Array(indentLevel + offset).join(INDENT);

    Spec.call(this, runner, options);
    chai.use(({Assertion}, utils) => {
        Assertion.overwriteMethod('assert', _super => function (...args) {
            const message = utils.flag(this, 'message');

            if (message) {
                assertions.push(message);
            }

            _super.apply(this, args);
        });
    });

    runner.on('suite', ({title}) => {
        ++indentLevel;
    });
    runner.on('suite end', ({title}) => {
        --indentLevel;
    });

    runner.on('pass', () => {
        assertions.forEach(displaySuccessfulAssertion(indent(2)));

        assertions = [];
    });
    runner.on('fail', () => {
        const passingAssertions = assertions.slice(0, assertions.length - 1);
        const failingAssertion = assertions.slice(-1)[0];

        passingAssertions.forEach(displaySuccessfulAssertion(indent(2)));
        displayFailingAssertion(indent(2), failingAssertion);

        assertions = [];
    });

}
utils.inherits(Spreeze, Spec);

module.exports = Spreeze;

