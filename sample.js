const mocha = require('mocha');
const {expect} = require('chai');

describe('Feature: Example of chai integration', () => {
    it('Scenario: running some sample assertions', () => {
        expect(1, 'should be 1 == 1').to.equal(1);
        expect(2).to.equal(2);
        expect(3, 'should be 3 == 3').to.equal(3);
    });

    it('Scenario: running more sample assertions', () => {
        expect(4, 'should be 4 == 4').to.equal(4);
        expect(4, 'should be 4 == 4').to.equal(4);
        expect(4, 'should be 4 == 3').to.equal(3);
    });
});
