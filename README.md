# Spreeze

## Description

The name _spreeze_ is in reference to the [blend of tea and coffee](https://en.wikipedia.org/w/index.php?title=Yuenyeung&oldid=761452855) served in Ethiopia.  As with the drink, this project combines Chai assertions with Mocha's "spec" reporter to produce meaningful output when tests are run.  By default, Chai assertion messages only display when an error condition is met, but for projects that depend on the output of tests to produce meaningful output the message may also be valuable in success conditions.

## Usage

### Execution of tests with the **spreeze** reporter
First, install via `npm install --save-dev spreeze` to persist to your project's `devDependencies`.

The `--reporter spreeze` argument can be provided via CLI arguments or as a line within **mocha.opts**.

### Crafting tests to utilize messages
This library deliberately targets the message parameter for Chai assertions, which can be exposed via assorted methods (e.g. [`.equal`](http://chaijs.com/api/bdd/#method_equal)).  Assertions that do not utilize this feature will not affect output.

For illustration, consider the accompanying [sample.js](https://gitlab.com/bellwether-open-source/spreeze/blob/master/sample.js) file for one potential style of test-writing that utilizes Chai messages to generate output.  From that file, the following line is expected to produce visible output under success conditions:

```js
expect(1, 'should be 1 == 1').to.equal(1);
```

...whereas the next line will not:

```js
expect(2).to.equal(2);
```
